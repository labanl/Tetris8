﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Tetris8.Piece;
using System.IO.IsolatedStorage;

namespace Tetris8
{
    public partial class MainPage : PhoneApplicationPage
    {
        bool started = false;
        UIControl _control;
        List<ScoreObject> scoreList = new List<ScoreObject>();

        public MainPage()
        {
            InitializeComponent();
        }

        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
            if (IsolatedStorageSettings.ApplicationSettings.Contains("scoreList"))
            {
                scoreList = IsolatedStorageSettings.ApplicationSettings["scoreList"] as List<ScoreObject>;

                if (scoreList != null)
                {
                    var tempList =
                    from score in scoreList
                    where score.Score > 0
                    orderby score.Score descending
                    select score;

                    lboScore.ItemsSource = null;
                    lboScore.ItemsSource = tempList.ToList(); ;
                }
            }

            this.Focus();

            newGame();
        }

        /// <summary>
        /// 初始化游戏
        /// </summary>
        void newGame()
        {
            _control = new UIControl();
            _control.GameOver += new EventHandler(_control_GameOver);
            this.DataContext = _control;

            canvasBox.Children.Clear();
            canvasBoxPrev.Children.Clear();

            foreach (Tetris8.Piece.Block block in _control.Container)
            {
                canvasBox.Children.Add(block);
            }

            foreach (Tetris8.Piece.Block block in _control.NextContainer)
            {
                canvasBoxPrev.Children.Add(block);
            }

            _control.Clear();
            _control.Score = 0;
            _control.Level = 0;
            _control.RemoveRowCount = 0;
        }

        /// <summary>
        /// 游戏结束事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _control_GameOver(object sender, EventArgs e)
        {
            btnPauseResume.Height = 0;
            btnGameOver.Height = 50;

            int count = Convert.ToInt32(lblScore.Text);
            count++;
            scoreList.Add(new ScoreObject { Score = count, Date = "Date: " + DateTime.Now.ToString() });

            if (IsolatedStorageSettings.ApplicationSettings.Contains("scoreList"))
                IsolatedStorageSettings.ApplicationSettings["scoreList"] = scoreList;
            else
                IsolatedStorageSettings.ApplicationSettings.Add("scoreList", scoreList);

            if (scoreList != null)
            {
                var tempList =
                from score in scoreList
                where score.Score > 0
                orderby score.Score descending
                select score;

                lboScore.ItemsSource = null;
                lboScore.ItemsSource = tempList.ToList(); ;
            }
        }

        private void uc_KeyDown(object sender, KeyEventArgs e)
        {
            if (_control.GameStatus != GameStatus.Play) return;

            if (e.Key == Key.Left)
            {
                _control.MoveToLeft();
            }
            else if (e.Key == Key.Right)
            {
                _control.MoveToRight();
            }
            else if (e.Key == Key.Up)
            {
                _control.Rotate();
            }
            else if (e.Key == Key.Down)
            {
                _control.MoveToDown();
            }
        }

        private void btnLeft_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _control.MoveToLeft();
        }

        private void btnDown_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _control.MoveToDown();
        }

        private void btnRight_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _control.MoveToRight();
        }

        private void btnChange_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _control.Rotate();
        }

        private void lboScore_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            lboScore.SelectedIndex = -1;
        }

        private void btnPauseResume_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (lblPauseResume.Text.ToString() == "resume")
            {
                _control.Play();
                lblPauseResume.Text = "pause";
            }
            else
            {
                _control.Pause();
                lblPauseResume.Text = "resume";
            }
        }

        private void pivot_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (pivot.SelectedIndex == 0)
            {
                gridPlay.Visibility = Visibility.Visible;
                gridScore.Visibility = Visibility.Collapsed;
            }
            else
            {
                gridPlay.Visibility = Visibility.Collapsed;
                gridScore.Visibility = Visibility.Visible;

                _control.Pause();
                lblPauseResume.Text = "resume";
            }
        }

        private void btnNewGame_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

            if (started)
            {
                if (_control.GameStatus == GameStatus.Over)
                {
                    newGame();

                    btnPauseResume.Height = 50;
                    lblPauseResume.Text = "pause";
                    btnGameOver.Height = 0;
                    _control.Play();
                }
                else
                {
                    _control.Pause();
                    lblPauseResume.Text = "resume";

                    if (MessageBox.Show("Do you want to start a new game?", "", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
                    {
                        _control.GameStatus = GameStatus.Over;
                        newGame();

                        btnPauseResume.Height = 50;
                        lblPauseResume.Text = "pause";
                        btnGameOver.Height = 0;
                        _control.Play();
                    }
                }
            }
            else
            {
                started = true;

                btnPauseResume.Height = 50;
                lblPauseResume.Text = "pause";
                btnGameOver.Height = 0;
                _control.Play();
            }
        }
    }

    public class ScoreObject
    {
        public int Score { get; set; }
        public string Date { get; set; }
    }
}
